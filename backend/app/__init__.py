from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
import pymysql
from config import config


pymysql.install_as_MySQLdb()

mail = Mail()
db = SQLAlchemy()

def create_app():
	app = Flask(__name__)
	app.config.from_object(config)
	
	mail.init_app(app)
	db.init_app(app)

	from .login_register import login_register as login_register_blueprint
	app.register_blueprint(login_register_blueprint)

	from .activity import activity as activity_blueprint
	app.register_blueprint(activity_blueprint)

	from .profile import profile as profile_blueprint
	app.register_blueprint(profile_blueprint)

	return app