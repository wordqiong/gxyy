from flask import Blueprint

activity = Blueprint('activity', __name__)

from . import create_activity
from . import get_keypoint
from . import activity_menu
from . import activity_info
from . import check_relation