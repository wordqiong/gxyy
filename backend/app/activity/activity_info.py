from flask import request
from . import activity
from ..models import User, Activity

@activity.route('/activity_info', methods = ['GET', 'POST'])
def activity_info():
    ret = {}
    if request.method == 'POST':
        context = request.get_json()
        id = context['activity_id']
        activity = Activity.query.filter_by(id = int(id)).first()
        user = User.query.filter_by(id = activity.owner_id).first()
        ret['creator_name'] = user.name
        ret['activity_name'] = activity.name
        activity_time = {}
        activity_time['year'] = activity.time.year
        activity_time['month'] = activity.time.month
        activity_time['day'] = activity.time.day
        ret['activity_time'] = activity_time
        activity_registration_deadline = {}
        activity_registration_deadline['year'] = activity.registration_deadline.year
        activity_registration_deadline['month'] = activity.registration_deadline.month
        activity_registration_deadline['day'] = activity.registration_deadline.day
        ret['activity_registration_deadline'] = activity_registration_deadline
        ret['activity_intro'] = activity.intro
        ret['activity_attention'] = activity.attention
        ret['num_of_register'] = len(activity.register_users)
        ret['num_of_favorite'] = len(activity.favorite_users)
    return ret

