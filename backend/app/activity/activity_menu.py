from flask import request
from . import activity
from .. import db
from ..models import Activity
from datetime import date

@activity.route('/activity_menu', methods = ['GET', 'POST'])
def activity_menu():
    ret = {}
    if request.method == 'GET':
        menu = []
        activities = db.session.query(Activity).order_by(Activity.time).all()
        print(activities)
        for activity in activities:
            if date.today() > activity.time:
                continue
            info = {}
            info['id'] = activity.id
            info['name'] = activity.name
            activity_date = {}
            activity_date['y'] = activity.time.year
            activity_date['m'] = activity.time.month
            activity_date['d'] = activity.time.day
            info['date'] = activity_date
            if date.today() <= activity.registration_deadline:
                info['state'] = 0
            elif date.today() < activity.registration_deadline:
                info['state'] = 1
            elif date.today() == activity.registration_deadline:
                info['state'] = 2
            menu.append(info)
        ret['menu'] = menu
    return ret

