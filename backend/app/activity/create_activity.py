from flask import request
from . import activity
from .. import db
from ..models import User, Activity, Keypoint
from datetime import date
import json

@activity.route('/create_activity', methods = ['GET', 'POST'])
def create_activity():
    if request.method == 'POST':
        context = request.get_json()
        print(context)
        uid = context['user_id']
        name = context['activity_name']
        time = date(context['activity_time']['year'], context['activity_time']['month'], context['activity_time']['day'])
        ddl = date(context['activity_registration_deadline']['year'], context['activity_registration_deadline']['month'], context['activity_registration_deadline']['day'])
        intro = context['activity_intro']
        attention = context['activity_attention']
        new_activity = Activity(name = name,
                                time = time,
                                registration_deadline = ddl,
                                intro = intro,
                                attention = attention)
        db.session.add(new_activity)
        db.session.commit()
        user = User.query.filter_by(id = uid).first()
        user.announced.append(new_activity)

        keypoints = context['keypoint_list']
        # keypoints = json.loads(context['keypoint_list'])
        
        for keypoint in keypoints:
            latitude = keypoint['latitude']
            longitude = keypoint['longitude']
            type = keypoint['type']
            new_keypoint = Keypoint(longitude = longitude,
                                    latitude = latitude,
                                    type = type,
                                    activity_id = new_activity.id)
            db.session.add(new_keypoint)
            db.session.commit()
            new_activity.keypoint.append(new_keypoint)
        return json.dumps({'status': 0})
    return json.dumps({'status': 1})