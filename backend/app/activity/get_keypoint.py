from flask import request
from . import activity
from ..models import Activity
import json

@activity.route('/get_keypoint', methods = ['GET', 'POST'])
def get_keypoint():
    ret = {}    
    if request.method == 'POST':
        context = request.get_json()
        activity_id = context['ActivityId']
        activity = Activity.query.filter_by(id = int(activity_id)).first()
        check = []
        supply = []
        for keypoint in activity.keypoint:
            point_info = {}
            point_info['latitude'] = float(keypoint.latitude)
            point_info['longitude'] = float(keypoint.longitude)
            point_info['id'] = keypoint.id
            if keypoint.type == 0:
                ret['Begin'] = point_info
            elif keypoint.type == 1:
                ret['Destination'] = point_info
            elif keypoint.type == 2:
                check.append(point_info)
            elif keypoint.type == 3:
                supply.append(point_info)
        ret['CheckInInfo'] = check
        ret['SupplyInfo'] = supply
    return json.dumps(ret)