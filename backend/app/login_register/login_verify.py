from flask import request
from . import login_register
from .. import db
from ..models import User

@login_register.route('/login_verify', methods = ['GET', 'POST'])
def login_verify():
    ret = {}
    ret['uid'] = 0
    if request.method == 'POST':
        context = request.get_json()
        name = context['name']
        password = context['password']
        # 查询
        user_list = User.query.filter_by(name = name).all()
        if len(user_list) == 0:
            ret['status'] = 2
        else :
            for user in user_list:
                # 验证密码
                if user.validate_password(password):
                    ret['status'] = 0
                    ret['uid'] = user.id
                else :
                    ret['status'] = 1
    return ret