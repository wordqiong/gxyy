from flask import request
from . import login_register
from ..email import send_email
from ..models import User, Verification
from .. import db
import random
import datetime

@login_register.route('/send_verification_code', methods = ['GET', 'POST'])
def mail_test():
    if request.method == 'POST':     
        context = request.get_json()
        addr = context['mailbox']
        # 邮箱是否已存在
        lst = User.query.filter_by(mail_address = addr).all()
        if len(lst) != 0:
            return {'status': 2}
        # 发送验证码
        code = str(random.randint(10000, 99999))[1:]
        send_email(addr, '【高校越野】注册验证码(5分钟内有效)', code)        
        lst = Verification.query.filter_by(mail_address = addr).all()
        # 更新数据库
        if len(lst) == 0:
            new_verification = Verification(mail_address = addr, 
                                            verification_code = code,
                                            send_time = datetime.datetime.now())
            db.session.add(new_verification)           
        else:
            lst[0].verification_code = code
            lst[0].send_time = datetime.datetime.now()
        db.session.commit()
        return {'status': 0}
    return {'status': 1}