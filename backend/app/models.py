from re import U
from . import db
from werkzeug.security import generate_password_hash, check_password_hash

# 关系表
favorites = db.Table('favorites', \
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')), \
    db.Column('activity_id', db.Integer, db.ForeignKey('activities.id')) \
)

register = db.Table('register', \
    db.Column('user_id', db.Integer, db.ForeignKey('users.id')), \
    db.Column('activity_id', db.Integer, db.ForeignKey('activities.id')) \
)

class Verification(db.Model):
    # 定义表名
    __tablename__ = 'verifications'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    mail_address = db.Column(db.String(32), unique=False, index=True)
    verification_code = db.Column(db.String(4))
    send_time = db.Column(db.DateTime) # xxxx - xx - xx

class User(db.Model):
    # 定义表名
    __tablename__ = 'users'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String(20), unique=True, index=True)
    password_hash = db.Column(db.String(128))
    mail_address = db.Column(db.String(32), unique=True, index=True)
    # 建立多对多关系
    favorites = db.relationship('Activity', secondary=favorites, back_populates='favorite_users')
    activities = db.relationship('Activity', secondary=register, back_populates='register_users')
    # 一对多属性
    announced = db.relationship('Activity', uselist=True, backref="owner", lazy='dynamic')

    def set_password(self, password):  # 用来设置密码的方法，接受密码作为参数
        self.password_hash = generate_password_hash(password)  # 将生成的密码保持到对应字段

    def validate_password(self, password):  # 用于验证密码的方法，接受密码作为参数
        return check_password_hash(self.password_hash, password)  # 返回布尔值

class Activity(db.Model):
    # 定义表名
    __tablename__ = 'activities'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String(30), index=True)
    time = db.Column(db.Date) # xxxx - xx - xx
    registration_deadline = db.Column(db.Date) # xxxx - xx - xx
    intro = db.Column(db.String(256))
    attention = db.Column(db.String(256))
    # 一对多属性
    keypoint = db.relationship("Keypoint", uselist=True, backref="activity", lazy='dynamic')
    # 建立多对多关系
    favorite_users = db.relationship('User', secondary=favorites, back_populates='favorites')
    register_users = db.relationship('User', secondary=register, back_populates='activities')
    # 外键
    owner_id = db.Column(db.Integer, db.ForeignKey(User.id))

class Keypoint(db.Model):
    # 定义表名
    __tablename__ = 'keypoints'
    # 定义字段
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    latitude = db.Column(db.DECIMAL(12,8))
    longitude = db.Column(db.DECIMAL(12,8))
    type = db.Column(db.Integer)
    # 外键
    activity_id = db.Column(db.Integer, db.ForeignKey(Activity.id))