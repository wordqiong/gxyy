from flask import Blueprint

profile = Blueprint('profile', __name__)

from . import add_favorite
from . import delete_favorite