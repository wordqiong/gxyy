package com.bug_gai_dui.gxyy;

import java.util.Date;

public class Activities {
    private int id;
    private String name;
    private int state;
    private Date time;
    public Activities(int id, String name, int state, Date time) {
        this.id=id;
        this.time=time;
        this.state=state;
        this.name=name;
    }
    public String getName() {
        return name;
    }
    public int getId() {
        return id;
    }
    public int getState() {
        return state;
    }
    public Date getTime() {
        return time;
    }
}
