package com.bug_gai_dui.gxyy;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import org.jetbrains.annotations.NotNull;
//import com.example.magicalpai.R;

import java.util.List;

public class ActivitiesAdapter extends RecyclerView.Adapter<ActivitiesAdapter.ViewHolder> {
    private Context mContext;
    private List<Activities> mActivityList;
    static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView activityName;
        TextView activityState;
        TextView activityTime;
        public ViewHolder(View view) {
            super(view);
            cardView = (CardView) view;
            activityName = (TextView) view.findViewById(R.id.activity_name);
            activityState = (TextView) view.findViewById(R.id.activity_state);
            activityTime = (TextView) view.findViewById(R.id.activity_time);
        }
    }
    public ActivitiesAdapter(List<Activities> activityList) {
        mActivityList = activityList;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.activities_item,parent, false);
        final ViewHolder holder = new ViewHolder(view);
        //为每一个子项的cardView注册事件
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                Activities activity = mActivityList.get(position);
                Toast.makeText(v.getContext(), "you clicked " + activity.getName(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(v.getContext(),DetailActivity.class);
                intent.putExtra("activity_id", activity.getId());
                v.getContext().startActivity(intent);
            }
        });
        return holder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Activities activity = mActivityList.get(position);
        holder.activityName.setText(activity.getName());
//        holder.activityState.setText(String.format("%d", activity.getState()));
        if(activity.getState()==0){
            holder.activityState.setText(" 报名中 ");
        }
        else if(activity.getState()==1){
            holder.activityState.setText(" 未开始 ");
        }
        else if(activity.getState()==2){
            holder.activityState.setText(" 进行中 ");
        }
        else if(activity.getState()==3){
            holder.activityState.setText(" 已结束 ");
        }
        else if(activity.getState()==4){
            holder.activityState.setText(" 未审核 ");
        }
        holder.activityTime.setText(activity.getTime().toString());
        /*
        Glide的用法：调用Glide.with()方法并传入一个Context、Activity或Fragment参数，然后调用load()方法去加载图片，
        可以是一个URL地址，也可以是一个本地路径，或者是一个资源id，最后调用into()方法将图片设置到具体某一个ImageView中就可以了
        Glide会对高清图片进行压缩，这样就不会发生内存溢出的问题了
        使用Glide，在app/build.grade文件中添加implementation 'com.github.bumptech.glide:glide:3.8.0即可
        */
//        Glide.with(mContext).load(fruit.getImageId()).into(holder.fruitImage);
    }
    @Override
    public int getItemCount() {
        return mActivityList.size();
    }
}

