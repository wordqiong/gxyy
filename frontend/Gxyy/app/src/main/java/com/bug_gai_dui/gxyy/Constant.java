package com.bug_gai_dui.gxyy;

public class Constant {
    public static final int DELTAMILLIS = 60000;

    //全局常量清单
    public enum KeyPointType{
        BEGIN,DESTINATION,SUPPLY,CHECKIN;
    };
    public enum UserStatus{
        TOURIST,PARTICIPANT,SPONSOR,NOT_INVOVED;//游客、活动参与者、活动发起者、没有参加到活动的用户
    }
    public final String[] name = {"起点","终点","补给点","打卡点"};
    public static final float VALID_DIS=25.0f;
}
