package com.bug_gai_dui.gxyy;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

public class DetailActivity extends AppCompatActivity {

    Integer userId;
    Integer activityId;
    String activityCreator;
    String activityName;
    String activityIntro;
    String activityAttention;
    Integer activityTimeYear;
    Integer activityTimeMonth;
    Integer activityTimeDay;
    Integer activityRegistrationDeadlineYear;
    Integer activityRegistrationDeadlineMonth;
    Integer activityRegistrationDeadlineDay;
    Boolean isRegistered;
    Boolean isFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        activityId = intent.getIntExtra("activity_id",0);

        LocalSQLite localSQLite = new LocalSQLite(DetailActivity.this,"UserInfo.db",null,1);
        userId = localSQLite.getUid();

        showActivityInfo();
        setButtonState();

        Button button_register = (Button) findViewById(R.id.detail_button_register);
        Button button_add_favorite = (Button) findViewById(R.id.detail_button_add_favorite);
        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        button_add_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isFavorite) {
                    deleteFavorite();
                } else {
                    addFavorite();
                }
            }
        });
    }

    void showActivityInfo() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("activity_id", activityId);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/activity_info") //获取活动信息接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(DetailActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                activityCreator = jsonObject.getString("creator_name");
                                activityName = jsonObject.getString("activity_name");
                                activityTimeYear = jsonObject.getJSONObject("activity_time").getInt("year");
                                activityTimeMonth = jsonObject.getJSONObject("activity_time").getInt("month");
                                activityTimeDay = jsonObject.getJSONObject("activity_time").getInt("day");
                                activityRegistrationDeadlineYear = jsonObject.getJSONObject("activity_registration_deadline").getInt("year");
                                activityRegistrationDeadlineMonth = jsonObject.getJSONObject("activity_registration_deadline").getInt("month");
                                activityRegistrationDeadlineDay = jsonObject.getJSONObject("activity_registration_deadline").getInt("day");
                                activityIntro = jsonObject.getString("activity_intro");
                                activityAttention = jsonObject.getString("activity_attention");

                                TextView textView_activity_name = (TextView) findViewById(R.id.detail_activity_name);
                                TextView textView_activity_creator = (TextView) findViewById(R.id.detail_activity_creator);
                                TextView textView_activity_time = (TextView) findViewById(R.id.detail_activity_time);
                                TextView textView_activity_intro = (TextView) findViewById(R.id.detail_activity_intro);
                                TextView textView_activity_attention = (TextView) findViewById(R.id.detail_activity_attention);
                                TextView textView_activity_registration_deadline = (TextView) findViewById(R.id.detail_activity_registration_deadline);
                                textView_activity_name.setText(activityName);
                                textView_activity_creator.setText(activityCreator);
                                textView_activity_time.setText(activityTimeYear+"-"+activityTimeMonth+"-"+activityTimeDay);
                                textView_activity_intro.setText(activityIntro);
                                textView_activity_attention.setText(activityAttention);
                                textView_activity_registration_deadline.setText(activityRegistrationDeadlineYear+"-"+activityRegistrationDeadlineMonth+"-"+activityRegistrationDeadlineDay);

                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }

    void setButtonState() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("user_id", userId);
                json.put("activity_id", activityId);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/check_relation")
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(DetailActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                isRegistered = jsonObject.getBoolean("is_registered");
                                isFavorite = jsonObject.getBoolean("is_favorite");

                                Button button_register = (Button) findViewById(R.id.detail_button_register);
                                Button button_add_favorite = (Button) findViewById(R.id.detail_button_add_favorite);
                                if(isRegistered) {
                                    button_register.setText("已报名");
                                    button_register.setClickable(false);
                                } else {
                                    button_register.setText("报名");
                                    button_register.setClickable(true);
                                }
                                if(isFavorite) {
                                    button_add_favorite.setText("已收藏");
                                } else {
                                    button_add_favorite.setText("收藏");
                                }
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }

    void addFavorite() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("user_id", userId);
                json.put("activity_id", activityId);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/add_favorite") //收藏活动接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(DetailActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                int status = jsonObject.getInt("status");

                                Button button_add_favorite = (Button) findViewById(R.id.detail_button_add_favorite);
                                if(status == 0) {
                                    button_add_favorite.setText("已收藏");
                                    Toast.makeText(DetailActivity.this,"收藏成功",Toast.LENGTH_SHORT).show();
                                    isFavorite = true;
                                } else if(status == 1) {
                                    button_add_favorite.setText("已收藏");
                                    Toast.makeText(DetailActivity.this,"已收藏",Toast.LENGTH_SHORT).show();
                                    isFavorite = true;
                                } else if(status == 2) {
                                    Toast.makeText(DetailActivity.this, "收藏失败", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }

    void deleteFavorite() {
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("user_id", userId);
                json.put("activity_id", activityId);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/delete_favorite") //收藏活动接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(DetailActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String res = null;
                            try {
                                res = Objects.requireNonNull(response.body()).string();
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(res);
                                int status = jsonObject.getInt("status");

                                Button button_add_favorite = (Button) findViewById(R.id.detail_button_add_favorite);
                                if(status == 0) {
                                    button_add_favorite.setText("收藏");
                                    Toast.makeText(DetailActivity.this,"取消收藏成功",Toast.LENGTH_SHORT).show();
                                    isFavorite = false;
                                } else if(status == 1) {
                                    button_add_favorite.setText("收藏");
                                    Toast.makeText(DetailActivity.this,"未收藏",Toast.LENGTH_SHORT).show();
                                    isFavorite = false;
                                } else if(status == 2) {
                                    Toast.makeText(DetailActivity.this, "取消收藏失败", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });
                }
            });
        }).start();
    }

}