package com.bug_gai_dui.gxyy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.Nullable;

public class LocalSQLite extends SQLiteOpenHelper {

    /*用法示例:
    初始化：
     LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);//第一步创建数据库帮助类
    方法：
      dbhelper.InsertUserInfo(int uid,String uname,String password); //插入用户信息
      dbhelper.getUserName();//返回最后登录的用户的用户名
      dbhelper.getPassword();//返回最后登录的用户的密码（目前明文）
      dp.helper.getUid();//返回最后登录用户的uid
     * */

    private Context mContext;
    public static final String dbName="UserInfo";
    public static final String CREATE_USER_INFO="create table UserInfo("
            +"keyword integer primary key autoincrement,"
            +"uid integer,"
            +"username text,"
            +"password text)"
    ;
    public LocalSQLite(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        mContext=context;
        Log.d("LocalSQLite", "helper");
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_INFO);
        Toast.makeText(mContext,"saved LoginInfo",Toast.LENGTH_SHORT).show();
        Log.d("LocalSQLite", "SQLcreate");
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d("LocalSQLite", "update");
    }
    public void InsertUserInfo(int uid,String uname,String password){
        //String sql = "insert into user(username,password) values ("+uname+","+password+")";//插入操作的SQL语句
        SQLiteDatabase db = getWritableDatabase();
        //db.execSQL(sql);//执行SQL语句
        ContentValues values=new ContentValues();
        values.put("uid",uid);
        values.put("username",uname);
        values.put("password",password);
        db.insert("UserInfo",null,values);

    }
    public void updateUsername(String username){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values=new ContentValues();
        Integer uid=getUid();

        values.put("username",username);
        db.update("UserInfo",values,"uid=?",new String[]{uid.toString()});
        Log.d(getUserName(), "updateUsername: ");
    }
    public String getUserName(){
        String uname=null;
        //自动查询最后一条数据
        //Cursor cursor =getWritableDatabase().rawQuery("select * from UserInfo order by id desc limit 1;",new String[] {"username", "password"});
        Cursor cursor =getWritableDatabase().query("UserInfo",new String[]{"uid","username","password"},null,null,null,null,"keyword desc","1");
        if(cursor.moveToFirst()) {
            uname = cursor.getString((int)cursor.getColumnIndex("username"));
            //Log.d("LocalSQLite", "getname");
        }
        cursor.close();
        return uname;
    }
    public String getPassword(){
        String pw=null;
        Cursor cursor =getWritableDatabase().query("UserInfo",new String[]{"uid","username","password"},null,null,null,null,"keyword desc","1");
        if(cursor.moveToFirst()) {
            pw = cursor.getString((int)cursor.getColumnIndex("password"));
            //Log.d("LocalSQLite", "getpassword");
        }
        cursor.close();
        return pw;
    }
    public int getUid(){
        int uid=0;
        Cursor cursor =getWritableDatabase().query("UserInfo",new String[]{"uid","username","password"},null,null,null,null,"keyword desc","1");
        if(cursor.moveToFirst()) {
            uid = cursor.getInt((int)cursor.getColumnIndex("uid"));
            Log.d("LocalSQLite", "getpassUid");
        }
        cursor.close();
        return uid;
    }
}
