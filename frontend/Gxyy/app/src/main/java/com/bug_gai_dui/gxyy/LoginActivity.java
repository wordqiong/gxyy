package com.bug_gai_dui.gxyy;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    String userName;
    String password;
    Integer userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // 点击“登录”按钮
        Button loginButton = findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        // 点击“注册”按钮跳转到注册界面
        Button registerButton = findViewById(R.id.register_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    protected void login(){
        EditText account_box = findViewById(R.id.account_box);
        EditText password_box = findViewById(R.id.password_box);
        userName = account_box.getText().toString().trim();
        password = password_box.getText().toString().trim();
        // 检查是否为空
        if(userName.length() == 0)
        {
            Toast.makeText(this,"请输入用户名",Toast.LENGTH_SHORT).show();
            return;
        }
        if(password.length() == 0)
        {
            Toast.makeText(this,"请输入密码",Toast.LENGTH_SHORT).show();
            return;
        }

        new Thread() {
            @Override
            public void run() {
                OkHttpClient okHttpClient = new OkHttpClient();
                
                MediaType JSON = MediaType.parse("application/json;charset=utf-8");
                JSONObject json = new JSONObject();
                try {
                    json.put("name", userName);
                    json.put("password", password);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }

                RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
                Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/login_verify")
                        .post(requestBody)
                        .build();

                Call call = okHttpClient.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(LoginActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String res = null;
                                try {
                                    res = Objects.requireNonNull(response.body()).string();
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                                try {
                                    JSONObject jsonObject = new JSONObject(res);
                                    if(jsonObject.getInt("status") == 0) {
                                        userId = jsonObject.getInt("uid");
                                        LocalSQLite dbhelper= new LocalSQLite(LoginActivity.this,"UserInfo.db",null,1);//第一步创建数据库帮助类
                                        dbhelper.InsertUserInfo(userId, userName, password); //插入用户信息
                                        Toast.makeText(LoginActivity.this, "登陆成功", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(LoginActivity.this,NavActivity.class);
                                        startActivity(intent);
                                    } else if (jsonObject.getInt("status") == 1) {
                                        Toast.makeText(LoginActivity.this, "密码错误", Toast.LENGTH_SHORT).show();
                                    } else if (jsonObject.getInt("status") == 2) {
                                        Toast.makeText(LoginActivity.this, "用户不存在", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                    }
                });
            }
        }.start();
    }
}