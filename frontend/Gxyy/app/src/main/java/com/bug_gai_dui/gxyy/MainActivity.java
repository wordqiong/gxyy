package com.bug_gai_dui.gxyy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

// 按钮
import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;

import android.os.Bundle;

//import com.example.magicalpai.R;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawerLayout;
    //private Fruit[] fruits = {new Fruit("Apple", R.drawable.apple), new Fruit("Banana",R.drawable.banana), new Fruit("Watermelon",R.drawable.watermelon)};
    //private List<Fruit> fruitList = new ArrayList<>();
    //private FruitAdapter adapter1;

    private Activities[] act = {
            new Activities(1, "aaa", 0, new Date(2000, 1, 1)),
            new Activities(2, "bbb", 1, new Date(2001, 1, 1)),
            new Activities(3, "ccc", 2, new Date(2002, 1, 1))
    };
    private List<Activities> actList = new ArrayList<>();
    private ActivitiesAdapter adapter;
    ImageButton imageButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initActivities();
        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        //GridLayoutManager的构造方法接收两个参数，第一个是Context,第二个是列数，这里我们希望每一行中会有两列数据
        GridLayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ActivitiesAdapter(actList);
        recyclerView.setAdapter(adapter);

        addListenerOnButton();
    }

    private void initActivities() {
        actList.clear();
        for (int i = 0; i < 3; i++) {
            Random random = new Random();
            int index = random.nextInt(act.length);
            actList.add(act[index]);
        }
    }
//    private void initFruits() {
//        fruitList.clear();
//        for (int i = 0; i < 50; i++) {
//            Random random = new Random();
//            int index = random.nextInt(fruits.length);
//            fruitList.add(fruits[index]);
//        }
//    }
    public void addListenerOnButton() {

        imageButton = (ImageButton) findViewById(R.id.plus_flow_button);

        imageButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Toast.makeText(MainActivity.this,
                        "Button is clicked!", Toast.LENGTH_SHORT).show();

            }

        });

    }
}


//package com.bug_gai_dui.gxyy;
//
//import androidx.appcompat.app.AppCompatActivity;
//import android.os.Bundle;
//// 按钮
//import android.app.Activity;
//import android.os.Bundle;
//import android.widget.ImageButton;
//import android.widget.Toast;
//import android.view.View;
//import android.view.View.OnClickListener;
//
//public class MainActivity extends AppCompatActivity {
//
//    ImageButton imageButton;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        addListenerOnButton();
//    }
//
//    public void addListenerOnButton() {
//
//        imageButton = (ImageButton) findViewById(R.id.plus_flow_button);
//
//        imageButton.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//
//                Toast.makeText(MainActivity.this,
//                        "Button is clicked!", Toast.LENGTH_SHORT).show();
//
//            }
//
//        });
//
//    }
//}

