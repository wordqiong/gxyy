package com.bug_gai_dui.gxyy;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps2d.*;
import com.amap.api.maps2d.model.*;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class OrienteeringActivity extends AppCompatActivity implements LocationSource, AMapLocationListener
{
    //TODO:进入本界面之前，传递本次定向越野活动的唯一id、用户名、用户身份？
    //信息结构体。考虑放到头文件？如果有的话
    class TeammateInfo{
        LatLng Position;
        String Uname;
        Marker marker;
        String TeamName;
        int TeamId;

    }
    class PointInfo{
        int id;
        Boolean is_checked;
        LatLng Position;
        Marker marker;
        public PointInfo(int i,LatLng ll){id=i;Position=ll;is_checked=false;marker=null;}
        public PointInfo(int i,LatLng ll,Boolean checked){id=i;Position=ll;is_checked=checked;marker=null;}
        public PointInfo(PointInfo x){id=x.id;Position=x.Position;is_checked=x.is_checked;marker=null;};
        public void ChangeMarker(Marker m){
            marker=m;
        }
    }

    //AMap是地图对象
    private AMap aMap;
    private MapView mapView;
    CheckPointCard cardWindow=null;
    //声明AMapLocationClient类对象，定位发起端
    private AMapLocationClient aMapLocationClient = null;
    //声明mLocationOption对象，定位参数
    public AMapLocationClientOption aMapLocationClientOption = null;
    //声明mListener对象，定位监听器
    private OnLocationChangedListener onLocationChangedListener = null;
    double MyLatitude;
    double  MyLongitude;
    //标识，用于判断是否只显示一次定位信息和用户重新定位
    private boolean isFirstLoc = true;
    private Context context = this;
    /*打开页面时需要获取的用户信息和活动信息*/
    private int ActivityId;
    private long TimeStart;
    private long TimeEnd;
    private String ActivityTitle;
    private int UserId=0;
    private String UserName="";
    private Constant.UserStatus uStatus= Constant.UserStatus.NOT_INVOVED;
    private LatLng Begin=new LatLng(31.282233,121.217469);
    private LatLng Destination=new LatLng(31.290008,121.214809);
    private ArrayList<PointInfo> Supplys = new ArrayList<>();//补给点坐标
    private ArrayList<PointInfo> CheckIns = new ArrayList<>();//打卡点坐标
    private ArrayList<TeammateInfo> Teammates = new ArrayList<>();//队友信息及坐标
    private ArrayList<LatLng> MyLatLng=new ArrayList<>();//自身位置信息（以备绘制轨迹）

    private Map<Integer,PointInfo> CheckPointMap=new HashMap<Integer,PointInfo>();
    private TextClock tc_timeText_12;
    // private ImageView welcomeImg = null;
    String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    List<String> mPermissionList = new ArrayList<>();
    private static final int PERMISSION_REQUEST = 1;
    Chronometer chronometer ;



    private Handler handler =new Handler();
    // 检查权限
    private void checkPermission() {
        mPermissionList.clear();
        //判断哪些权限未授予
        for (int i = 0; i < permissions.length; i++) {
            if (ContextCompat.checkSelfPermission(this, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permissions[i]);
            }
        }
        /**
         * 判断是否为空
         */
        if (mPermissionList.isEmpty()) { //未授予的权限为空，表示都授予了
        } else { //请求权限方法
            String[] permissions = mPermissionList.toArray(new String[mPermissionList.size()]);//将List转为数组
            ActivityCompat.requestPermissions(OrienteeringActivity.this, permissions, PERMISSION_REQUEST);
        }
    }
    /**
     * 响应授权
     * 这里不管用户是否拒绝，都进入首页，不再重复申请权限
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST:
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }
    private void initView() {


        tc_timeText_12 = findViewById(R.id.tc_timeText_12);
        //setTimeZone使用(UTC-7)无效,
        //原因:源码未对UTC+(-)进行处理，下面有具体的源码分析
        tc_timeText_12.setTimeZone("China/Beijing");//有效
        //tc_timeText_12.setTimeZone("GMT+7:00");//有效
        //tc_dateText_12.setTimeZone("GMT+7:00");//有效
        tc_timeText_12.setFormat24Hour("HH:mm:ss");
        chronometer.setCountDown(true);

        chronometer.setBase(TimeEnd);
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            private String format(long time){

                long hourRemain = time%(24*60*60*1000);
                String hour = hourRemain/3600000>9?hourRemain/3600000+"":"0"+hourRemain/3600000;
                long minRemain = hourRemain%3600000;
                String min = minRemain/60000>9?minRemain/60000+"":"0"+minRemain/60000;
                long secRemain = minRemain%60000;
                String sec = secRemain/1000>9?secRemain/1000+"":"0"+secRemain/1000;
                return hour+":"+min+":"+sec;
            }
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long base = chronometer.getBase();
                long cur1 = System.currentTimeMillis();
                long l = base - cur1;
                if(l<=0){
                    chronometer.stop();
                    chronometer.setText("活动已结束");
                }else {
                    chronometer.setText(format(l));
                }
            }
        });
        chronometer.start();
    }
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //检查权限
        checkPermission();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orienteering);
        //TODO:页面连接好后再调试
        //Get the Intent that started this activity and extract the string
       // Intent intent = getIntent();
       // uStatus = (Constant.UserStatus) intent.getExtras().get("uStatus");
       // UserId=intent.getExtras().getString("UserId");
       // ActivityId=intent.getExtras().getInt("ActivityId");
        //ActivityTitle=intent.getExtras().getString("ActivityTitle");
        LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);
        ActivityId=124;
        ActivityTitle="高校越野0204";
        UserName=dbhelper.getUserName();
        UserId=dbhelper.getUid();//?
        TimeEnd =System.currentTimeMillis()+60000;

        uStatus=Constant.UserStatus.PARTICIPANT;
        /*  画地图   */
        AMapLocationClient.updatePrivacyShow(context,true,true);
        AMapLocationClient.updatePrivacyAgree(context,true);
        //获取地图控件引用
        mapView = (MapView) findViewById(R.id.map);
        chronometer=  (Chronometer) findViewById(R.id.chronometer);
        TextView txt_title=findViewById(R.id.ActivityTitle);
        txt_title.setText(ActivityTitle);
        //在activity执行onCreate时执行mMapView.onCreate(savedInstanceState)，实现地图生命周期管理
        mapView.                                                                                                                                                                                                                                                        onCreate(savedInstanceState);
        if (aMap == null) {
            aMap = mapView.getMap();
        }
        initView();
        MyLocationStyle myLocationStyle;
        myLocationStyle = new MyLocationStyle();//初始化定位蓝点样式类myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE);//连续定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动。（1秒1次定位）如果不设置myLocationType，默认也会执行此种模式。
        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATE);
        myLocationStyle.interval(2000); //设置连续定位模式下的定位间隔，只在连续定位模式下生效，单次定位模式下不会生效。单位为毫秒。
        //myLocationStyle.showMyLocation(false);
        //myLocationStyle.myLocationIcon(BitmapDescriptorFactory.fromResource(R.drawable.gps));
        aMap.setMyLocationStyle(myLocationStyle);//设置定位蓝点的Style
        aMap.moveCamera(CameraUpdateFactory.zoomTo(17));
        aMap.getUiSettings().setMyLocationButtonEnabled(true);//设置默认定位按钮是否显示，非必需设置。
        aMap.setMyLocationEnabled(true);// 设置为true表示启动显示定位蓝点，false表示隐藏定位蓝点并不进行定位，默认是false。
        /*      画出起点终点补给点和打卡点     */
        aMap.setOnInfoWindowClickListener(mInfoWindowListener);

        aMap.setOnMapClickListener(mMapClickListener);
        askForKeyPoint();//从后端获取
        drawKeyPoint();//绘制关键点
        /*    持续更新位置信息          */
       handler.post(sendAndGetPosition);//每隔一分钟提交并记录自身位置、拉取队友位置并绘制

    }

    /*画出所有关键点 起终点打卡点和补给点*/
    private void drawKeyPoint() {
        //UpdatePosition();
        aMap.addMarker(new MarkerOptions().position(Begin).title("起点").snippet(" "));
        aMap.addMarker(new MarkerOptions().position(Destination).title("终点").snippet(" "));
        for(PointInfo checkIn:CheckIns){
            MarkerOptions markerOption = new MarkerOptions();
            markerOption.position(checkIn.Position);
            markerOption.title("打卡点").snippet("请点击此处进入打卡界面");
            markerOption.draggable(false);
            //最好是画个图标
            Marker marker = aMap.addMarker(markerOption);
            CheckPointMap.put(marker.hashCode(),new PointInfo(checkIn));//利用marker自带的键值与信息绑定
            Log.d("drawKeyPoint", "drawKeyPoint: "+checkIn.Position.toString());
        }
        for(PointInfo supply:Supplys){
            MarkerOptions markerOption = new MarkerOptions();
            markerOption.title("补给点");
            markerOption.position(supply.Position);
            markerOption.draggable(false);
            //markerOption.icon();
            Marker marker = aMap.addMarker(markerOption);
        }
        //队友位置
        //TODO：改逻辑
        for(TeammateInfo teammate:Teammates){
            MarkerOptions markerOption = new MarkerOptions();
            markerOption.position(teammate.Position);
            markerOption.title(teammate.Uname).snippet("队友");
            markerOption.draggable(false);
            //markerOption.icon();
            Marker marker = aMap.addMarker(markerOption);
        }
        aMap.invalidate();//刷新地图
        Log.d("drawKeyPoint", "drawKeyPoint: "+aMap.getMapScreenMarkers().size());
    }

    AMap.OnMapClickListener mMapClickListener=new AMap.OnMapClickListener() {
        @Override
        public void onMapClick(LatLng latLng) {
            Log.d("windowD", "onMapClick: mapc");
            //TODO:删除打卡窗体
            if(cardWindow!=null) {
                LinearLayout linearLayout = findViewById(R.id.linearLayout);
                linearLayout.removeView(cardWindow);
            }
        }
    };
    AMap.OnInfoWindowClickListener mInfoWindowListener = new AMap.OnInfoWindowClickListener() {
        @Override
        public void onInfoWindowClick(Marker marker) {
            if(marker.getTitle().equals("打卡点")){
                if(uStatus== Constant.UserStatus.PARTICIPANT) {
                    //动态添加子视图（card)
                    Log.d("windowD", "onMapClick: infoC");
                    LinearLayout linearLayout = findViewById(R.id.linearLayout);
                    if(cardWindow==null) {
                        Log.d("windowD", "onMapClick: newCard");

                        cardWindow = new CheckPointCard(context, CheckPointMap.get(marker.hashCode()));
                        linearLayout.addView(cardWindow, -1);
                    }
                    else{
                        linearLayout.removeView(cardWindow);
                        cardWindow = new CheckPointCard(context, CheckPointMap.get(marker.hashCode()));
                        linearLayout.addView(cardWindow, -1);
                    }

                }
                else if(uStatus== Constant.UserStatus.SPONSOR){
                    Intent intent = new Intent(OrienteeringActivity.this,CheckPointActivity.class);
                    intent.putExtra("uStatus",uStatus);
                    intent.putExtra("CheckPointId", Objects.requireNonNull(CheckPointMap.get(marker.hashCode())).id);
                    intent.putExtra("ActivityId",ActivityId);
                    intent.putExtra("ActivityTitle",ActivityTitle);
                    startActivity(intent);
                }
            }
        }
    };

    /*请求后端，更新位置信息*/
    private void UpdatePosition(){
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                ActivityId=12345;//姑且随便写了
                UserId = 67890;
                json.put("ActivityId",ActivityId);
                json.put("UserId",UserId);
                //用户当前位置
                JSONObject posObj=new JSONObject();
                posObj.put("latitude",MyLatitude);
                posObj.put("longitude",MyLongitude);
                json.put("UserPosition",posObj);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url("http://127.0.0.1:5000/UpdatePosition") //创建活动接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(OrienteeringActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    String res = Objects.requireNonNull(response.body()).string();
                    //解析传回的JSON
                    JSONObject jObject= null;
                    try {
                        JSONObject aStruct=null;
                        jObject = new JSONObject(res);
                        JSONArray Arr=jObject.getJSONArray("TeammateInfo");
                        for (int i = 0; i < Arr.length(); i++) {
                            aStruct =Arr.getJSONObject(i);//得到数组中对应下标对应的json对象
                            LatLng ll = new LatLng(aStruct.getDouble("latitude"),aStruct.getDouble("longitude"));
                            CheckIns.add(new PointInfo(i,ll));
                        }

                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    //这是干什么用的？
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(OrienteeringActivity.this, "更新位置成功", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            });
        }).start();
    }
    /*动态更新位置信息 1分钟更新1次*/
    private final Runnable sendAndGetPosition = new Runnable(){
        public void run(){
            try {
                //prepare and send the data here..
                UpdatePosition();
                handler.postDelayed(this, 60000);//一分钟更新一次位置信息
                //获取地图上所有Marker,找到属于的部分并改变位置
                List<Marker> mapScreenMarkers = aMap.getMapScreenMarkers();
                for (int i = 0; i < mapScreenMarkers.size(); i++) {
                    Marker marker = mapScreenMarkers.get(i);
                    if (Objects.equals(marker.getSnippet(), "队友")) {
                        for(TeammateInfo t:Teammates){
                            if(t.Uname.equals(marker.getTitle())){
                                marker.setPosition(t.Position);
                            }
                        }
                    }
                }
                aMap.invalidate();//刷新地图
//                runOnUiThread(new Runnable() {
//                    public void run() {
//
//                    }
//                }
            ;}
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    public class CheckPointCard extends LinearLayout{
        public PointInfo check_point;
        public CheckPointCard(Context context,PointInfo c_point) {
            super(context);
//            Log.d("windowD", "CheckPointCard: ok");
            //定义布局
            View insideView=LayoutInflater.from(context).inflate(R.layout.activity_orienteering_checkpoint,this);
            Button btn_checkpoint=(Button) findViewById(R.id.btn_checkpoint);
            TextView txt_disctence=(TextView) findViewById(R.id.txt_distance);
            Log.d("windowD", "CheckPointCard: ?");
            //用错变量了……根本不报的
            if(c_point.is_checked){
                btn_checkpoint.setText("当前打卡点已打卡成功！");
                btn_checkpoint.setActivated(false);
            }
            Log.d("windowD", "CheckPointCard: ??");
            check_point = c_point;
            //TextView=
            btn_checkpoint.setOnClickListener(new OnClickListener() {
                //设置监听器
                public void onClick(View v) {
                    Location myll=aMap.getMyLocation();
                    float distance = AMapUtils.calculateLineDistance(new LatLng(myll.getLatitude(),myll.getLongitude()),check_point.Position);
                    if(distance<=Constant.VALID_DIS){
                        //打卡成功
                        //请求后端并登记
                        if(AskForCheckIn(check_point.id)==1) {
                            btn_checkpoint.setText("当前打卡点已打卡成功！");
                            btn_checkpoint.setActivated(false);
                            //将is_checked置1
                            for (int mk : CheckPointMap.keySet()) {
                                if (CheckPointMap.get(mk) == c_point) {
                                    Objects.requireNonNull(CheckPointMap.get(mk)).is_checked = true;
                                    break;
                                }
                            }
                        }
                        else
                            Toast.makeText(context,"网络请求失败", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(context,"距离打卡点"+distance+"m，打卡失败!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }


    }
//进行打卡
    private int AskForCheckIn(int pointId) {
        final int[] FuncRet = {0};
        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("UserId",UserId);
                json.put("ActivityId",ActivityId);
                json.put("CheckPointId",pointId);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/CheckIn") //创建活动接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(OrienteeringActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    String res = Objects.requireNonNull(response.body()).string();
                    //解析传回的JSON
                    if(res.equals("ok")) {
                        Objects.requireNonNull(CheckPointMap.get(pointId)).is_checked = true;
                        FuncRet[0] =1;
                    }

                    //这是干什么用的？
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(OrienteeringActivity.this, "打卡成功", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });
                }
            });
        }).start();
        return FuncRet[0];
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mapView.onDestroy();
        aMapLocationClient.stopLocation();//停止定位
        aMapLocationClient.onDestroy();//销毁定位客户端。
        handler.removeCallbacks(sendAndGetPosition);//停止循环发送
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView.onResume ()，实现地图生命周期管理
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView.onPause ()，实现地图生命周期管理
        mapView.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行mMapView.onSaveInstanceState (outState)，实现地图生命周期管理
        mapView.onSaveInstanceState(outState);
    }
    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null) {
            if (aMapLocation.getErrorCode() == 0) {
                //定位成功回调信息，设置相关消息
                aMapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见官方定位类型表
                aMapLocation.getLatitude();//获取纬度
                aMapLocation.getLongitude();//获取经度
                MyLatitude=aMapLocation.getLatitude();
                MyLongitude= aMapLocation.getLongitude();//获取经度
                aMapLocation.getAccuracy();//获取精度信息
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date(aMapLocation.getTime());
                df.format(date);//定位时间
                aMapLocation.getAddress();//地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
                aMapLocation.getCountry();//国家信息
                aMapLocation.getProvince();//省信息
                aMapLocation.getCity();//城市信息
                aMapLocation.getDistrict();//城区信息
                aMapLocation.getStreet();//街道信息
                aMapLocation.getStreetNum();//街道门牌号信息
                aMapLocation.getCityCode();//城市编码
                aMapLocation.getAdCode();//地区编码

                // 如果不设置标志位，此时再拖动地图时，它会不断将地图移动到当前的位置
                if (isFirstLoc) {
                    //设置缩放级别
                    aMap.moveCamera(CameraUpdateFactory.zoomTo(17));
                    //将地图移动到定位点
                    aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude())));
                    //点击定位按钮 能够将地图的中心移动到定位点
                    onLocationChangedListener.onLocationChanged(aMapLocation);
                    //添加图钉
                    //  aMap.addMarker(getMarkerOptions(amapLocation));
                    //获取定位信息
                    StringBuffer buffer = new StringBuffer();
                    buffer.append(aMapLocation.getCountry() + ""
                            + aMapLocation.getProvince() + ""
                            + aMapLocation.getCity() + ""
                            + aMapLocation.getProvince() + ""
                            + aMapLocation.getDistrict() + ""
                            + aMapLocation.getStreet() + ""
                            + aMapLocation.getStreetNum());
                    Toast.makeText(getApplicationContext(), buffer.toString(), Toast.LENGTH_LONG).show();
                    isFirstLoc = false;
                }
            } else {
                //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
                Log.e("AmapError", "location Error, ErrCode:"
                        + aMapLocation.getErrorCode() + ", errInfo:"
                        + aMapLocation.getErrorInfo());
                Toast.makeText(getApplicationContext(), "定位失败", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        this.onLocationChangedListener = onLocationChangedListener;
    }
    @Override
    public void deactivate() {
        onLocationChangedListener = null;
    }
    /*请求后端 获取关键点*/
    public void askForKeyPoint() {
        //TODO:调试点
        CheckIns.add(new PointInfo(1,new LatLng(31.290203,121.218469),false));

        new Thread(() -> {
            OkHttpClient okHttpClient = new OkHttpClient();

            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {

                json.put("ActivityId",ActivityId);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }

            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/KeyPoint") //创建活动接口
                    .post(requestBody)
                    .build();

            Call call = okHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(OrienteeringActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    String res = Objects.requireNonNull(response.body()).string();
                    //解析传回的JSON
                    JSONObject jObject= null;
                    try {
                        JSONObject aStruct=null;
                        jObject = new JSONObject(res);
                        TimeStart=jObject.getLong("TimeStart");
                        TimeEnd=jObject.getLong("TimeEnd");
                        aStruct=jObject.getJSONObject("Destination");
                        Destination=new LatLng(aStruct.getDouble("latitude"),aStruct.getDouble("longitude"));
                        aStruct=jObject.getJSONObject("Begin");
                        Begin=new LatLng(aStruct.getDouble("latitude"),aStruct.getDouble("longitude"));
                        JSONArray Arr=jObject.getJSONArray("CheckInInfo");
                        for (int i = 0; i < Arr.length(); i++) {
                            aStruct =Arr.getJSONObject(i);//得到数组中对应下标对应的json对象
                            LatLng ll = new LatLng(aStruct.getDouble("latitude"),aStruct.getDouble("longitude"));
                            int id = aStruct.getInt("id");
                            CheckIns.add(new PointInfo(id,ll,false));//只有checkPoint需要这个参数
                        }
                        Arr=jObject.getJSONArray("SupplyInfo");
                        for (int i = 0; i < Arr.length(); i++) {
                            aStruct =Arr.getJSONObject(i);//得到数组中对应下标对应的json对象
                            LatLng ll = new LatLng(aStruct.getDouble("latitude"),aStruct.getDouble("longitude"));
                            int id = aStruct.getInt("id");
                            Supplys.add(new PointInfo(id,ll));

                        }

                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(OrienteeringActivity.this, "获取关键点成功", Toast.LENGTH_SHORT).show();
                                    }
                                });

                        }
                    });
                }
            });
        }).start();

    }
}

