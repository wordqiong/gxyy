package com.bug_gai_dui.gxyy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

// 按钮
import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;

import android.os.Bundle;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.example.magicalpai.R;
import java.io.IOException;
import java.util.*;

public class PublishedActivity extends AppCompatActivity {
    String username;
    private DrawerLayout mDrawerLayout;
    //private Fruit[] fruits = {new Fruit("Apple", R.drawable.apple), new Fruit("Banana",R.drawable.banana), new Fruit("Watermelon",R.drawable.watermelon)};
    //private List<Fruit> fruitList = new ArrayList<>();
    //private FruitAdapter adapter1;

    private Activities[] act = {
            new Activities(1, "ddd", 0, new Date(2000, 1, 1)),
            new Activities(2, "eee", 1, new Date(2001, 1, 1)),
            new Activities(3, "fff", 2, new Date(2002, 1, 1))
    };
    private List<Activities> actList = new ArrayList<>();
    private ActivitiesAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect);

        LocalSQLite dbhelper= new LocalSQLite(this,"UserInfo.db",null,1);//第一步创建数据库帮助类
        dbhelper.getUserName(); //返回最后登录的用户的用户名

        username=dbhelper.getUserName();
        initActivities();

        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("已发布");

        //GridLayoutManager的构造方法接收两个参数，第一个是Context,第二个是列数，这里我们希望每一行中会有两列数据
        GridLayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ActivitiesAdapter(actList);
        recyclerView.setAdapter(adapter);

    }

//    private void initActivities() {
//        actList.clear();
//        for (int i = 0; i < 3; i++) {
//            Random random = new Random();
//            int index = random.nextInt(act.length);
//            actList.add(act[index]);
//        }
//    }

    public void initActivities() {
        new Thread(()->{

            // @Headers({"Content-Type:application/json","Accept: application/json"})//需要添加头
            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
            JSONObject json = new JSONObject();
            try {
                json.put("username", username);

            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
            //申明给服务端传递一个json串
            //创建一个OkHttpClient对象
            OkHttpClient okHttpClient = new OkHttpClient();
            //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
            //json为String类型的json数据
            RequestBody requestBody = RequestBody.create(String.valueOf(json),JSON);
            //创建一个请求对象
//                        String format = String.format(KeyPath.Path.head + KeyPath.Path.waybillinfosensor, username, key, current_timestamp);
            Request request = new Request.Builder()
                    .url(getResources().getString(R.string.sever)+"/published_activity") //创建活动接口
                    .post(requestBody)
                    .build();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(PublishedActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                }

                            });
                        }
                    });   }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    String res = Objects.requireNonNull(response.body()).string();
                    //解析传回的JSON
                    JSONObject jObject= null;
                    try {
                        JSONObject aStruct=null;
                        jObject = new JSONObject(res);
                        JSONArray Arr=jObject.getJSONArray("menu");
                        actList.clear();
                        for (int i = 0; i < Arr.length(); i++) {
                            aStruct =Arr.getJSONObject(i);//得到数组中对应下标对应的json对象
                            Activities ac = new Activities(aStruct.getInt("id"),
                                    aStruct.getString("name"),
                                    aStruct.getInt("state"),
                                    new Date(aStruct.getJSONObject("date").getInt("y"),aStruct.getJSONObject("date").getInt("m"),aStruct.getJSONObject("date").getInt("d")));
                            actList.add(ac);
                        }
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                    //这是干什么用的？
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(PublishedActivity.this, "获取活动成功", Toast.LENGTH_SHORT).show();
                                }
                            });

                        }
                    });
                }
            });


        }).start();
    }
}



