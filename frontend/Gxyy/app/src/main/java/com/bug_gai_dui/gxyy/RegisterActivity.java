package com.bug_gai_dui.gxyy;

import android.content.Intent;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

public class RegisterActivity extends AppCompatActivity {
    boolean isUserExist = true;
    boolean isPasswdCheck = false;
    boolean isPasswdConfirm = false;
    boolean isVerificationCodeSend = false;

    EditText userName;
    EditText password;
    EditText confirmPassword;
    EditText editText_mailbox;
    EditText editText_verification_code;
    Button registerButton;
    Button button_send_verification_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        userName = (EditText) findViewById(R.id.account_box);
        password = (EditText) findViewById(R.id.password_box);
        confirmPassword = (EditText) findViewById(R.id.password_check_box);
        editText_mailbox = (EditText) findViewById(R.id.editText_mailbox);
        editText_verification_code = (EditText) findViewById(R.id.editText_verification_code);
        registerButton = (Button) findViewById(R.id.register_button);
        button_send_verification_code = (Button) findViewById(R.id.button_send_verification_code);

        userName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if(!focus) {
                    if (userName.getText().toString().length() == 0) {
                        Toast.makeText(RegisterActivity.this, "请输入用户名", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    new Thread() {
                        @Override
                        public void run() {
                            OkHttpClient okHttpClient = new OkHttpClient();

                            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
                            JSONObject json = new JSONObject();
                            try {
                                json.put("user_name", userName.getText().toString().trim());
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }

                            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
                            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/check_name")
                                    .post(requestBody)
                                    .build();

                            Call call = okHttpClient.newCall(request);
                            call.enqueue(new Callback() {
                                @Override
                                public void onFailure(Call call, IOException e) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(RegisterActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                                @Override
                                public void onResponse(Call call, final Response response) throws IOException {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            String res = null;
                                            try {
                                                res = Objects.requireNonNull(response.body()).string();
                                            } catch (IOException e) {
                                                throw new RuntimeException(e);
                                            }
                                            try {
                                                JSONObject jsonObject = new JSONObject(res);
                                                if (jsonObject.getInt("status") == 0) {
                                                    isUserExist = false;
                                                } else if (jsonObject.getInt("status") == 1) {
                                                    isUserExist = true;
                                                    Toast.makeText(RegisterActivity.this, "用户名已存在", Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (JSONException e) {
                                                throw new RuntimeException(e);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }.start();
                }
            }
        });

        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if(!focus) {
                    String pwd = password.getText().toString().trim();
                    if(pwd.length() < 8 || pwd.length() > 16) {
                        Toast.makeText(RegisterActivity.this, "密码长度应为8~16位", Toast.LENGTH_SHORT).show();
                        isPasswdCheck = false;
                        isPasswdConfirm = false;
                        return;
                    }
                    int hasNum = 0;
                    int hasLow = 0;
                    int hasUpper = 0;
                    int hasSpecial = 0;
                    for(int i = 0; i < pwd.length(); i++) {
                        if(pwd.charAt(i) >= '0' && pwd.charAt(i) <= '9') {
                            hasNum = 1;
                        } else if(pwd.charAt(i) >= 'a' && pwd.charAt(i) <= 'z') {
                            hasLow = 1;
                        } else if(pwd.charAt(i)>='A' && pwd.charAt(i) <= 'Z') {
                            hasUpper = 1;
                        } else
                            hasSpecial = 1;
                    }
                    if(hasNum + hasLow + hasUpper + hasSpecial < 3) {
                        Toast.makeText(RegisterActivity.this, "密码请至少包含大写字母、小写字母、数字、特殊符号中的三种", Toast.LENGTH_SHORT).show();
                        isPasswdCheck = false;
                        isPasswdConfirm = false;
                        return;
                    }
                    isPasswdCheck = true;
                    if(confirmPassword.getText().toString().trim().length() != 0 && password.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
                        isPasswdConfirm = true;
                    } else {
                        isPasswdConfirm = false;
                    }
                }
            }
        });

        confirmPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if(!focus) {
                    if(password.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
                        isPasswdConfirm = true;
                    } else {
                        isPasswdConfirm = false;
                        Toast.makeText(RegisterActivity.this, "两次密码不同", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(password.getText().toString().trim().equals(confirmPassword.getText().toString().trim())) {
                    isPasswdConfirm = true;
                } else {
                    isPasswdConfirm = false;
                    Toast.makeText(RegisterActivity.this, "两次密码不同", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (editText_verification_code.getText().toString().trim().length() == 0) {
                    Toast.makeText(RegisterActivity.this, "请填写验证码", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!isUserExist && isPasswdCheck && isPasswdConfirm && isVerificationCodeSend) {
                    new Thread() {
                        @Override
                        public void run() {
                            OkHttpClient okHttpClient = new OkHttpClient();

                            MediaType JSON = MediaType.parse("application/json;charset=utf-8");
                            JSONObject json = new JSONObject();
                            try {
                                json.put("user_name", userName.getText().toString().trim());
                                json.put("password", password.getText().toString().trim());
                                json.put("mailbox",editText_mailbox.getText().toString().trim());
                                json.put("verification_code",editText_verification_code.getText().toString().trim());
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }

                            RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
                            Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/create_account")
                                    .post(requestBody)
                                    .build();

                            Call call = okHttpClient.newCall(request);
                            call.enqueue(new Callback() {
                                @Override
                                public void onFailure(Call call, IOException e) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(RegisterActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                                @Override
                                public void onResponse(Call call, final Response response) throws IOException {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            String res = null;
                                            try {
                                                res = Objects.requireNonNull(response.body()).string();
                                            } catch (IOException e) {
                                                throw new RuntimeException(e);
                                            }
                                            try {
                                                JSONObject jsonObject = new JSONObject(res);
                                                if(jsonObject.getInt("status") == 0) {
                                                    Toast.makeText(RegisterActivity.this, "注册成功", Toast.LENGTH_SHORT).show();
                                                    Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                                                    startActivity(intent);
                                                } else if (jsonObject.getInt("status") == 1) {
                                                    Toast.makeText(RegisterActivity.this, "验证码错误或失效", Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (JSONException e) {
                                                throw new RuntimeException(e);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }.start();
                } else {
                    Toast.makeText(RegisterActivity.this, "请检查输入", Toast.LENGTH_SHORT).show();
                }
            }
        });

        button_send_verification_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendVerificationCode();
            }
        });
    }

    void sendVerificationCode() {
        if (editText_mailbox.getText().toString().trim().length() == 0) {
            Toast.makeText(RegisterActivity.this, "请输入邮箱", Toast.LENGTH_SHORT).show();
            return;
        }
        new Thread() {
            @Override
            public void run() {
                OkHttpClient okHttpClient = new OkHttpClient();

                MediaType JSON = MediaType.parse("application/json;charset=utf-8");
                JSONObject json = new JSONObject();
                try {
                    json.put("mailbox",editText_mailbox.getText().toString().trim());
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }

                RequestBody requestBody = RequestBody.create(String.valueOf(json), JSON);
                Request request = new Request.Builder().url(getResources().getString(R.string.sever) + "/send_verification_code")
                        .post(requestBody)
                        .build();

                Call call = okHttpClient.newCall(request);
                call.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(RegisterActivity.this, "服务器错误", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String res = null;
                                try {
                                    res = Objects.requireNonNull(response.body()).string();
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                                try {
                                    JSONObject jsonObject = new JSONObject(res);
                                    if (jsonObject.getInt("status") == 0) {
                                        Toast.makeText(RegisterActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                        isVerificationCodeSend = true;
                                        CountDownTimer countDownTimer = new CountDownTimer(60 * 1000,1000) {
                                            @Override
                                            public void onTick(long l) {
                                                button_send_verification_code.setText(l / 1000 + "s 后重试");
                                            }

                                            @Override
                                            public void onFinish() {
                                                button_send_verification_code.setClickable(true);
                                                button_send_verification_code.setText("发送验证码");
                                                cancel();
                                            }
                                        };
                                        countDownTimer.start();
                                        button_send_verification_code.setClickable(false);
                                    } else if (jsonObject.getInt("status") == 1) {
                                        Toast.makeText(RegisterActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
                                    } else if (jsonObject.getInt("status") == 2) {
                                        Toast.makeText(RegisterActivity.this, "此邮箱已被注册", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        });
                    }
                });
            }
        }.start();
    }
}