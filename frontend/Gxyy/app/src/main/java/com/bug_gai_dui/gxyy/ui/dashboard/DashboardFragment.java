package com.bug_gai_dui.gxyy.ui.dashboard;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.bug_gai_dui.gxyy.*;
import com.bug_gai_dui.gxyy.databinding.FragmentHomeBinding;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

public class DashboardFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private SwipeRefreshLayout swipeRefreshLayout;
    private DashboardViewModel DashboardViewModel;
    private View view;
    private ActivitiesAdapter adapter;
    private List<Activities> actList = new ArrayList<>();
    private SwipeRefreshLayout refreshP;
    private RecyclerView recyclerView;
    private ImageButton imageButton;
    private SearchView mSearchView;
    //    private Activities[] act;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setTitle("我的活动");//设置主标题名称
//        toolbar.setSubtitle("副标题");//设置副标题名称
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_home_black_24dp);//是左边的图标样式
//        setHasOptionsMenu(true);

        initActivities();
        //mSearchView = (SearchView) view.findViewById(R.id.search_view);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_view);
        //GridLayoutManager的构造方法接收两个参数，第一个是Context,第二个是列数，这里我们希望每一行中会有两列数据
        //GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        WrapContentLinearLayoutManager layoutManager = new WrapContentLinearLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ActivitiesAdapter(actList);
        recyclerView.setAdapter(adapter);

        refreshP = view.findViewById(R.id.swipeRefresh);
        refreshP.setOnRefreshListener(this);

        imageButton = (ImageButton) view.findViewById(R.id.plus_flow_button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),CreateActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

    private void initActivities() {
        NavActivity navActivity = (NavActivity) getActivity();
        navActivity.getActivityInfo();
        actList = com.bug_gai_dui.gxyy.NavActivity.activityList;
    }

    @Override
    public void onRefresh() {
        refreshP.postDelayed(new Runnable() { // 发送延迟消息到消息队列
            @Override
            public void run() {
                initActivities();
                //adapter.notifyDataSetChanged();
                adapter = new ActivitiesAdapter(actList);
                recyclerView.setAdapter(adapter);
                refreshP.setRefreshing(false); // 是否显示刷新进度;false:不显示
            }
        },3000);
    }
}


//package com.bug_gai_dui.gxyy.ui.dashboard;
//
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//import androidx.lifecycle.Observer;
//import androidx.lifecycle.ViewModelProvider;
//import com.bug_gai_dui.gxyy.databinding.FragmentDashboardBinding;
//
//public class DashboardFragment extends Fragment {
//
//    private DashboardViewModel dashboardViewModel;
//    private FragmentDashboardBinding binding;
//
//    public View onCreateView(@NonNull LayoutInflater inflater,
//                             ViewGroup container, Bundle savedInstanceState) {
//        dashboardViewModel =
//                new ViewModelProvider(this).get(DashboardViewModel.class);
//
//        binding = FragmentDashboardBinding.inflate(inflater, container, false);
//        View root = binding.getRoot();
//
//        final TextView textView = binding.textDashboard;
//        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
//        return root;
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        binding = null;
//    }
//}