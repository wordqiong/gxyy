package com.bug_gai_dui.gxyy.ui.home;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.bug_gai_dui.gxyy.*;
import com.bug_gai_dui.gxyy.databinding.FragmentHomeBinding;
import okhttp3.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private SwipeRefreshLayout swipeRefreshLayout;
    private HomeViewModel homeViewModel;
    private View view;
    private ActivitiesAdapter adapter;
    private List<Activities> actList = new ArrayList<>();
    private SwipeRefreshLayout refreshP;
    private RecyclerView recyclerView;
    private ImageButton imageButton;
    private SearchView mSearchView;
    //    private Activities[] act;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

//        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
//        toolbar.setTitle("");//设置主标题名称
//        toolbar.setSubtitle("副标题");//设置副标题名称
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_home_black_24dp);//是左边的图标样式
//        setHasOptionsMenu(true);

        initActivities();
        mSearchView = (SearchView) view.findViewById(R.id.search_view);
        recyclerView=(RecyclerView)view.findViewById(R.id.recycler_view);
        //GridLayoutManager的构造方法接收两个参数，第一个是Context,第二个是列数，这里我们希望每一行中会有两列数据
        //GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        WrapContentLinearLayoutManager layoutManager = new WrapContentLinearLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ActivitiesAdapter(actList);
        recyclerView.setAdapter(adapter);

        refreshP = view.findViewById(R.id.swipeRefresh);
        refreshP.setOnRefreshListener(this);
//        refreshP.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                new Runnable() {
//                    @Override
//                    public void run() {
//                        initActivities();
//                        //adapter.notifyDataSetChanged();
//                        adapter = new ActivitiesAdapter(actList);
//                        recyclerView.setAdapter(adapter);
//                        refreshP.setRefreshing(false);
//                    }
//                };
//            }
//        });

        imageButton = (ImageButton) view.findViewById(R.id.plus_flow_button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),CreateActivity.class);
                startActivity(intent);
            }
        });

        //搜索

//        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            // 当点击搜索按钮时触发该方法
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                return false;
//            }
//
//            // 当搜索内容改变时触发该方法
////            @Override
////            public boolean onQueryTextChange(String newText) {
////                if (!TextUtils.isEmpty(newText)){
////                    lListView.setFilterText(newText);
////                }else{
////                    lListView.clearTextFilter();
////                }
////                return false;
////            }
//        });

        return view;
    }

    //    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_name, menu);
//    }

//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        ((AppCompatActivity) getActivity()).getMenuInflater().inflate(R.menu.menu_main, menu);
//        SearchManager searchManager =
//                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView =
//                (SearchView) menu.findItem(R.id.ab_search).getActionView();
//        searchView.setSearchableInfo(
//                searchManager.getSearchableInfo(getComponentName()));
//        return true;
//    }



    private void initActivities() {
        NavActivity navActivity = (NavActivity) getActivity();
        navActivity.getActivityInfo();
        actList = com.bug_gai_dui.gxyy.NavActivity.activityList;
//        Activities[] act = {
//                new Activities(1, "aaa", 0, new Date(2000, 1, 1)),
//                new Activities(2, "bbb", 1, new Date(2001, 1, 1)),
//                new Activities(3, "ccc", 2, new Date(2002, 1, 1))
//        };
//        actList.clear();
//        int n = 3;
//        for (int i = 0; i < n; i++) {
//            Random random = new Random();
//            int index = random.nextInt(act.length);
//            actList.add(act[index]);
//        }
    }

    @Override
    public void onRefresh() {
        refreshP.postDelayed(new Runnable() { // 发送延迟消息到消息队列
            @Override
            public void run() {
                initActivities();
                //adapter.notifyDataSetChanged();
                adapter = new ActivitiesAdapter(actList);
                recyclerView.setAdapter(adapter);
                refreshP.setRefreshing(false); // 是否显示刷新进度;false:不显示
            }
        },3000);
    }
}