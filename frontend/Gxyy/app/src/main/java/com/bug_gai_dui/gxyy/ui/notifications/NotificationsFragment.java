package com.bug_gai_dui.gxyy.ui.notifications;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.bug_gai_dui.gxyy.*;
import com.bug_gai_dui.gxyy.databinding.FragmentNotificationsBinding;

public class NotificationsFragment extends Fragment {

    private NotificationsViewModel notificationsViewModel;
    private FragmentNotificationsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
//        notificationsViewModel =
//                new ViewModelProvider(this).get(NotificationsViewModel.class);
//        final TextView textView = binding.textNotifications;
//        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
        //建了一些接口 ，目前author wyh
        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        String name;
        TextView name_set;
        LocalSQLite dbhelper= new LocalSQLite(getActivity(),"UserInfo.db",null,1);//第一步创建数据库帮助类
        dbhelper.getUserName();//返回最后登录的用户的用户名
        name_set=root.findViewById(R.id.get_user_name);
        name=dbhelper.getUserName();
        name_set.setText(name);
        //查看个人详细信息的页面跳转
        LinearLayout gethistory=root.findViewById(R.id.user_home_head);
        gethistory.setFocusable(false);
        gethistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ModifyUser.class);
                startActivity(intent);
            }});

        //查看历史记录的页面跳转
        LinearLayout getUserHead=root.findViewById(R.id.get_user_history);
        getUserHead.setFocusable(false);
        getUserHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HistoryActivity.class);
                startActivity(intent);
            }});

        //已收藏的页面跳转
        LinearLayout haveStore=root.findViewById(R.id.get_user_have_store);

        haveStore.setFocusable(false);
        haveStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CollectActivity.class);
                startActivity(intent);
            }});
        //已发布的页面跳转
        LinearLayout havePost=root.findViewById(R.id.get_user_have_post);

        havePost.setFocusable(false);
        havePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PublishedActivity.class);
                startActivity(intent);
            }});
        LinearLayout havebaoming=root.findViewById(R.id.get_user_have_baoming);

        havebaoming.setFocusable(false);
        havebaoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SignedActivity.class);
                startActivity(intent);
            }});
        LinearLayout needProcess=root.findViewById(R.id.get_user_need_process);

        needProcess.setFocusable(false);
        needProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UnauditActivity.class);
                startActivity(intent);
            }});

        NavActivity navActivity=(NavActivity) getActivity();
        navActivity.getUserInfo();
        boolean isGM= NavActivity.isGM;
        if(isGM==false)
        {
            needProcess.setVisibility(View.INVISIBLE);
            LinearLayout need_hide=root.findViewById(R.id.useless_to_tongyi);
            need_hide.setVisibility(View.INVISIBLE);
        }
        return root;
        }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}